# Game of Life


This is a simple Swing project demonstrating the [Conway's Game of Life](https://www.wikiwand.com/en/Conway%27s_Game_of_Life).

![Game of Life Screenshot](images/Game_of_Life_screenshot.png)



## How to build and run the app

1. Install [JDK](https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK_Howto.html) and [maven](https://www.baeldung.com/install-maven-on-windows-linux-mac) on your system
2. In terminal navigate to the project directory (where the `pom.xml` file is located) and run
	```
	mvn package	
	```
3. And then run
	```
	java -jar target/game_of_life-1.0-SNAPSHOT-jar-with-dependencies.jar
	```

where `1.0-SNAPSHOT` is the version of the executable (pick the latest).

---

## References

**Tutorial:** <https://www.youtube.com/watch?v=VNeXtrN7Kyg>

