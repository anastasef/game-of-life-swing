package game;

public class GameClock extends Thread {
	
	public static boolean running = true;

	@Override
	public void run() {
		
		while (running) {
			try {
				sleep(100);
				Game.nextGeneration();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	

}
