package game;

import java.util.concurrent.ThreadLocalRandom;

import gui.Gui;

public class Game {
	
	public static final int CELL_COUNT = Gui.GRIDSQUARE_COUNT;
	public static boolean[][] cells = new boolean[CELL_COUNT][CELL_COUNT];
	public static boolean[][] tmp = new boolean[CELL_COUNT][CELL_COUNT];
	
	int startCells = CELL_COUNT * 20;
	static int generation = 0;
	
	public void seed() {
		
		for (int i = 0; i < startCells; i++) {
			int x = rand(0, CELL_COUNT);
			int y = rand(0, CELL_COUNT);
			
			cells[x][y] = true;
		}
	}
	
	/**
	 * Calculate the next generation of cells based of each cell's
	 * neighbours count.
	 */
	public static void nextGeneration() {
		generation++;
		
		int neighbours = 0;
		
		//  Check the entire grid
		for (int r = 0; r < CELL_COUNT; r++) {
			for (int c = 0; c < CELL_COUNT; c++) {
				
				neighbours = neighbours(r, c);
				
				if (cells[r][c]) {
					if (neighbours < 2 || neighbours > 3) {
						tmp[r][c] = false;
					} else {
						tmp[r][c] = true;
					}
														
				} else {
					if (neighbours == 3) {
						tmp[r][c] = true;
					} else {
						tmp[r][c] = false;
					}
					
				}
			}
		}
		
		// Copy temp grid to the original
		for (int r = 0; r < CELL_COUNT; r++) {
			for (int c = 0; c < CELL_COUNT; c++) {
				cells[r][c] = tmp[r][c];
			}
		}
	}
	
	/**
	 * Count current cell's surrounding 8 neighbours
	 * @param x position of a checked cell
	 * @param y position of a checked cell
	 * @return number of live neighbours
	 */
	public static int neighbours(int x, int y) {
		int count = 0;
		
		for (int r = x - 1; r <= x + 1; r++) { 
			for (int c = y - 1; c <= y + 1; c++) { 
				// check for array bounds
				// and ignore checking the given cell
				if (r < 0 || c < 0 || r >= cells.length || c >= cells.length
					|| r == x && c == y) continue;
				if (cells[r][c]) count++;
			}
		}
		
		return count;
	}
	
	public static int rand(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max);
	}

}
