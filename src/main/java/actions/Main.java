package actions;

import game.Game;
import game.GameClock;
import gui.Gui;

public class Main {
    public static void main(String[] args) {
        
    	Gui gui = new Gui();
    	Game game = new Game();
    	GameClock clock = new GameClock();
    	
    	game.seed();
    	gui.create();
    	clock.start();
    }
}
