package gui;

import javax.swing.JFrame;

public class Gui {
	
	JFrame jf;
	public static Draw d;
	
	public static final int GRID_SQUARE_SIDE = 10, GRIDSQUARE_COUNT = 60;
	public static final int OFFSET = GRID_SQUARE_SIDE; // window padding
	public static final int WINDOW_SIDE = (GRID_SQUARE_SIDE * GRIDSQUARE_COUNT) + (OFFSET * 7);
	
	public void create() {
		jf = new JFrame("Game of Life");
		jf.setSize(WINDOW_SIDE, WINDOW_SIDE);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLocationRelativeTo(null);
		jf.setResizable(false);
		
		d = new Draw();
		d.setBounds(0, 0, WINDOW_SIDE, WINDOW_SIDE);
		d.setVisible(true);
		
		jf.add(d);
		
		jf.setVisible(true);
	}
}
