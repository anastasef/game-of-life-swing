package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.JLabel;

import game.Game;

public class Draw extends JLabel {

	Point paintCoordinates = new Point(0, 0);
	
	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING, 
				RenderingHints.VALUE_ANTIALIAS_OFF);
		
		// draw the grid
//		g.setColor(Color.WHITE);
//		for (int x = 0; x < Gui.GRIDSQUARE_COUNT; x++) {
//			for (int y = 0; y < Gui.GRIDSQUARE_COUNT; y++) {
//				paintCoordinates = pointToCoordinate(x, y);
//				
//				g.drawRect(
//						paintCoordinates.x, paintCoordinates.y,
//						Gui.GRID_SQUARE_SIDE, Gui.GRID_SQUARE_SIDE);
//			}
//		}
		
		// draw living and dead cells
		for (int x = 0; x < Game.CELL_COUNT; x++) {
			for (int y = 0; y < Game.CELL_COUNT; y++) {
				
				paintCoordinates = pointToCoordinate(x, y);
				
				// fill living cells
				if (Game.cells[x][y]) {
					g.setColor(new Color(0, 102, 204));
					g.fillRect(
							paintCoordinates.x + 1, paintCoordinates.y + 1, 
							Gui.GRID_SQUARE_SIDE - 1, Gui.GRID_SQUARE_SIDE - 1);
				} else { // fill dead cells
					g.setColor(new Color(255, 255, 255));
					g.fillRect(
							paintCoordinates.x + 1, paintCoordinates.y + 1, 
							Gui.GRID_SQUARE_SIDE - 1, Gui.GRID_SQUARE_SIDE - 1);
				}
			}
		}
		
		repaint();
	}
	
	private Point pointToCoordinate(int pw, int ph) {
		Point p = new Point(0, 0);
		
		p.x = pw * Gui.GRID_SQUARE_SIDE + Gui.OFFSET;
		p.y = ph * Gui.GRID_SQUARE_SIDE + Gui.OFFSET;
		
		return p;
	}
}
