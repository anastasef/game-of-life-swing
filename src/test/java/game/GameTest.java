package game;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gui.Gui;

class GameTest {

	Gui gui = new Gui();
	Game game = new Game();
	
	/**
	 * Create a blinker pattern
	 * 	| 0 1 0 |
	 * 	| 0 1 0 |
	 * 	| 0 1 0 |
	 */
	@BeforeEach
	@AfterEach
	private void seed() {
		game.cells[0][0] = false;
		game.cells[0][1] = true;
		game.cells[0][2] = false;
		
		game.cells[1][0] = false;
		game.cells[1][1] = true;
		game.cells[1][2] = false;
		
		game.cells[2][0] = false;
		game.cells[2][1] = true;
		game.cells[2][2] = false;
	}
	
	@Test
	@DisplayName("Positive test for counting cell's neighbours of a blinker pattern")
	void testNeighbours() {
		assertEquals(2, game.neighbours(0, 0));
		assertEquals(1, game.neighbours(0, 1));
		assertEquals(2, game.neighbours(0, 2));
		
		assertEquals(3, game.neighbours(1, 0));
		assertEquals(2, game.neighbours(1, 1));
		assertEquals(3, game.neighbours(1, 2));
		
		assertEquals(2, game.neighbours(2, 0));
		assertEquals(1, game.neighbours(2, 1));
		assertEquals(2, game.neighbours(2, 2));
	}
	
	/**
	 * The pattern should become
	 * 	| 0 0 0 |
	 * 	| 1 1 1 |
	 * 	| 0 0 0 |
	 */
	@Test
	@DisplayName("Positive test for calculating the first generation for a blinker pattern")
	void testFirstNextGeneration() {
		game.nextGeneration();
		
		assertEquals(false, game.cells[0][0]);
		assertEquals(false, game.cells[0][1]);
		assertEquals(false, game.cells[0][2]);
		
		assertEquals(true, game.cells[1][0]);
		assertEquals(true, game.cells[1][1]);
		assertEquals(true, game.cells[1][2]);
		
		assertEquals(false, game.cells[2][0]);
		assertEquals(false, game.cells[2][1]);
		assertEquals(false, game.cells[2][2]);
	}
	
	/**
	 * The pattern should become
	 * 	| 0 1 0 |
	 * 	| 0 1 0 |
	 * 	| 0 1 0 |
	 */
	@Test
	@DisplayName("Positive test for calculating the second generation for a blinker pattern")
	void testSecondNextGeneration() {
		game.nextGeneration();
		game.nextGeneration();
		
		assertEquals(false, game.cells[0][0]);
		assertEquals(true, game.cells[0][1]);
		assertEquals(false, game.cells[0][2]);
		
		assertEquals(false, game.cells[1][0]);
		assertEquals(true, game.cells[1][1]);
		assertEquals(false, game.cells[1][2]);
		
		assertEquals(false, game.cells[2][0]);
		assertEquals(true, game.cells[2][1]);
		assertEquals(false, game.cells[2][2]);
	}

}
